// Créez une classe Produit : nom & prix

// Créez une classe Panier avec : une méthode ajoute( produit ) / une méthode retire( produit )
// une proprieté totalHT  / une proprieté totalTTC

// Utilisation : ajouter ce qu'il faut à ce code de base pour qu'il fonctionne.

function Produit (nom, prix ){      // ceci est une classe avec (paramètre)
    this.nom = nom;                 // prorpriété /
    this.prix = prix ;              // prorpriété
}

function Panier(){

    this.produits = [] ;         //  propriété  
    this.totalHT = 0;                       
    this.totalTTC = 0 ;
    this.TVA = 1.055 ;

    this.ajoute = function(prix){           // ceci est une methode 
        // console.log("Ajoute" + nom.nom );
        // this.totalHT = this.totalHT + nom.prix ; 
        this.produits.push(prix) ;          // mettre les produit dans la liste
        this.totalHT = this.totalHT+prix.prix ; // ajouter la TVA au prix 
        this.totalTTC = this.totalHT * 1.055 ;
    }
    this.retire = function(prix){
        console.log("Ajoute" - prix.prix);
        this.totalHT = this.totalHT - prix.prix ; 
    };
    
}
function Viennoiserie (nom,prix,frais){

    Produit.call(this,nom,prix);
    this.frais = Boolean(true);  
}


var baguette = new Produit( 'Baguette', 0.85); // prix HT
var croissant = new Produit( 'Croissant', 0.80);
var chocolatine = new Viennoiserie ('chocolatine', 1.05);

var panier = new Panier();
// panier.ajoute(baguette);
// console.log(panier);
// panier.ajoute(croissant);
panier.ajoute(chocolatine);
console.log(panier)

// console.log(panier.totalHT);
// console.log(panier.totalTTC); 
console.log(chocolatine)
